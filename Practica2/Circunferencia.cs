﻿using System;

namespace Circunferencia
{
    class Program
    {
        

        static void Main(string[] args)
        {
            const double pi = 3.1416;
            Console.WriteLine("Escribe el radio de la circunferencia: ");
            double radio =double.Parse(Console.ReadLine()); //Aqui tuve que hacer un cast para que un valor de tipo string valla a double
            double resultado = pi * radio * radio;

            Console.WriteLine("El area del circulo es de: " + resultado);

            Console.WriteLine("Para calcular el area del circulo debemos obtener el diametro");
            Console.WriteLine("Recuerda que el diametro se mide multiplicando dos veces el radio" );
            Console.WriteLine();
            double diametro = radio * 2; // para calcular el diametro ya teniamos calculado el radio, asi que solo lo multiplicamos x2
            double resultado2 = diametro * pi;

            Console.WriteLine($"El perimetro del circulo es de: {resultado2}");








        }
    }
}