﻿using System;

namespace RuedaMoneda
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A continuacion se mostraran los datos para rueda: ");
            Console.WriteLine();
            Program.Rueda();
            Console.WriteLine("A continuacion se mostraran los datos para moneda: ");
            Program.Moneda(); 
        }
        static void Rueda() {
            const double Pi = 3.1416;
            Console.WriteLine("Escribe el radio de la rueda: ");
            double radio = double.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("El perimetro de la rueda segun el radio es :");
            double perimetro = (2 * radio) * Pi;
            Console.WriteLine($"El perimetro de la rueda es: {perimetro}");
            Console.WriteLine();
            Console.WriteLine();
            double Area = Pi * (radio * radio);
            Console.WriteLine($"El area de la rueda segun los datos es: {Area}");
            Console.WriteLine();
            Console.WriteLine("-----------------------------------------------------------------------");



        }
        static void Moneda()
        {
            const double Pi = 3.1416;
            Console.WriteLine("Escribe el radio de la moneda: ");
            double radio = double.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("El perimetro de la moneda segun el radio");
            double perimetro = (2 * radio) * Pi;
            Console.WriteLine($"El perimetro de la moneda es: {perimetro}");
            Console.WriteLine();
            Console.WriteLine();
            double Area = Pi * (radio * radio);
            Console.WriteLine($"El area de la rueda segun los datos es: {Area}");
        }
    }
}
