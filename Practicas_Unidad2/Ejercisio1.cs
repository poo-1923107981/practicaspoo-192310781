﻿using System;

namespace Practica2_Unidad2
{
    class Program
    {
        class Operacion
        {
            int numero = 1;

            public int GeneraPares(int pares)
            {
                //var i = 1;
                for (var numero = 1; numero < pares;)
                {
                    int r = (numero * 2);
                    Console.WriteLine(r);
                    numero++;

                }
                return numero;

            }
            public void ArregloPares()
            {
                //int[] Pares;
                Console.WriteLine("Cuantos pares tendra tu arreglo?");
                //Pares = new int[int.Parse(Console.ReadLine())];
                int[] Pares = { int.Parse(Console.ReadLine()) };
                Console.WriteLine();
                Console.WriteLine("Tu arreglo tiene estos pares: ");
                Console.WriteLine(Pares);

            }

        }
        static void Main(string[] args)
        {
            // Imprimir pares y sumar los arreglos de los pares.
            // "n" yo lo llamare como "numero"

            Operacion pares = new Operacion();
            Console.WriteLine("Escribe cuantos pares quieres tener: ");
            pares.GeneraPares(int.Parse(Console.ReadLine()));

            Console.WriteLine();
            
            Console.WriteLine("Suma de arreglos");
            Console.WriteLine("Aqui va tu primer arreglo");
            pares.ArregloPares();
            Console.WriteLine("Aqui sigue tu segundo arreglo");
            pares.ArregloPares();

            
           

            //Operacion[] Arreglo = new Operacion[pares.GeneraPares(int.Parse(Console.ReadLine()))];
            
        }
    }
}
